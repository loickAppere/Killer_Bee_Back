﻿const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const db = require('_helpers/db');
const { secret } = require('config.json');

module.exports = {
    getAll,
    getById,
    getByEmail,
    create,
    hashPassword,
    update,
    delete: _delete,
    verifyToken,
    refreshToken
};

async function getAll() {
    return await db.User.findAll();
}

async function getById(id) {
    return await getUser(id);
}


async function create(params) {
    // validate
    if (await db.User.findOne({ where: { email: params.email } })) {
        throw 'Email "' + params.email + '" is already registered';
    }
    const user = new db.User(params);

    // save user
    await user.save();
}

async function update(id, params) {
    const user = await getUser(id);

    // validate
    const usernameChanged = params.username && user.username !== params.username;
    if (usernameChanged && await db.User.findOne({ where: { username: params.username } })) {
        throw 'Username "' + params.username + '" is already taken';
    }

    // hash password if it was entered
    if (params.password) {
        params.hash = await bcrypt.hash(params.password, 10);
    }

    // copy params to user and save
    Object.assign(user, params);
    await user.save();

    return omitHash(user.get());
}

async function _delete(id) {
    const user = await getUser(id);
    await user.destroy();
}

// helper functions

async function getUser(id) {
    const user = await db.User.findByPk(id);
    if (!user) throw 'User not found';
    return user;
}

async function getByEmail(email){
    const user = await db.User.findOne({where: {email: email}});
    if (!user) throw 'User not found';
    return user;
}

async function authenticate({ username, password }) {
    const user = await db.User.scope('withHash').findOne({ where: { username } });

    if (!user || !(await bcrypt.compare(password, user.hash)))
        throw 'Username or password is incorrect';

    // authentication successful
    const token = jwt.sign({ sub: user.id }, secret, { expiresIn: '7d' });
    return { ...omitHash(user.get()), token };
}


function omitHash(user) {
    const { hash, ...userWithoutHash } = user;
    return userWithoutHash;
}

function encryptCeasar(str, offset) {
    let newArray = [];
    for (let i = 0; i < str.length; i++) {
        if (str.charCodeAt(i) >= 65 && str.charCodeAt(i) <= 90) {
            newArray.push((((str.charCodeAt(i) - 65) + offset) % 26) + 65);
        }
        else {
            newArray.push((((str.charCodeAt(i) - 97) + offset) % 26) + 97);
        }
    }
    let newWords = "";
    for (let j = 0; j < newArray.length; j++) {
        newWords += String.fromCharCode(newArray[j])
    }
    return newWords;
}

function hashPassword(password){
    //Hash
    let hashPassword = password.split("").reverse().join("");
    let charHashPassword = hashPassword.split("")
    let hashPass1=[];
    let hashPass2=[];
    for(let i=0; i<charHashPassword.length/2;i++){
        hashPass1.push(charHashPassword[i]);
    }
    hashPass1=hashPass1.join("");
    let l = charHashPassword.length
    l=Math.ceil(l/2);
    for(let i= l;i<charHashPassword.length;i++){
        hashPass2.push(charHashPassword[i])
    }
    hashPass2=hashPass2.join("")
    hashPass1 = encryptCeasar(hashPass1, 3);
    hashPass2 = encryptCeasar(hashPass2, 5);

    hashPassword= hashPass1.concat(hashPass2);
    return hashPassword
}

async function verifyToken(req, res, next){
    let token = req.headers.token;
    if(token == null) return res.sendStatus(401);
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, decoded) => {
        if(err) return res.sendStatus(403);
        req.email = decoded.email;
        next();
    })
}

async function refreshToken(req,res,next){
    try {
        const refreshToken = req.cookies.refreshToken;
        if(!refreshToken) return res.sendStatus(401);
        const user = await Users.findAll({
            where:{
                refresh_token: refreshToken
            }
        });
        if(!user[0]) return res.sendStatus(403);
        jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, decoded) => {
            if(err) return res.sendStatus(403);
            const userId = user[0].id;
            const name = user[0].name;
            const email = user[0].email;
            const accessToken = jwt.sign({userId, name, email}, process.env.ACCESS_TOKEN_SECRET,{
                expiresIn: '15s'
            });
            res.json({ accessToken });
        });
    } catch (error) {
        console.log(error);
    }
}
