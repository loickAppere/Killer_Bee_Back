const { DataTypes } = require('sequelize');
const Joi = require("joi");

module.exports = process;

function process(sequelize) {

    const attributes = {
        id: {type: DataTypes.INTEGER, allowNull: false, primaryKey: true, autoIncrement: true},
        name: { type: DataTypes.STRING, allowNull: false },
        description: { type: DataTypes.STRING, allowNull: false },
        steps: {type: DataTypes.TEXT, allowNull: false},
        ModelId: { type: DataTypes.INTEGER, allowNull: true}
    };

    return sequelize.define('Process', attributes);
}
