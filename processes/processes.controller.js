const express = require('express');
const router = express.Router();
const Joi = require('joi');

const validateRequest = require('_middleware/validate-request');
const processService = require('./process.service');
const verifyToken = require('../users/user.service');

// routes

router.get('/', verifyToken.verifyToken, getAll);
router.get('/:id', verifyToken.verifyToken,getById);
router.post('/', createSchema, create);
router.put('/:id', updateSchema, update);
router.delete('/:id', _delete);

module.exports = router;

// route functions

function getAll(req, res, next) {
    processService.getAll()
        .then(processes => res.json(processes))
        .catch(next);
}

function getById(req, res, next) {
    processService.getById(req.params.id)
        .then(process => res.json(process))
        .catch(next);
}

function create(req, res, next) {
    processService.create(req.body)
        .then(() => res.json({ message: 'process created' }))
        .catch(next);
}

function update(req, res, next) {
    processService.update(req.params.id, req.body)
        .then(() => res.json({ message: 'process updated' }))
        .catch(next);
}

function _delete(req, res, next) {
    processService.delete(req.params.id)
        .then(() => res.json({ message: 'process deleted' }))
        .catch(next);
}

// schema functions

function createSchema(req, res, next) {
    const schema = Joi.object({
        name: Joi.string().required(),
        steps: Joi.string().required(),
        description: Joi.string().required(),
        ModelId: Joi.number().required()

    });
    validateRequest(req, next, schema);
}

function updateSchema(req, res, next) {
    const schema = Joi.object({
        name: Joi.string().empty(''),
        steps: Joi.string().empty(''),
        description: Joi.string().empty(''),
        ModelId: Joi.number().empty('')
    });
    validateRequest(req, next, schema);
}
