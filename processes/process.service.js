const bcrypt = require('bcryptjs');

const db = require('_helpers/db');

module.exports = {
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

async function getAll() {
    return await db.Process.findAll();
}

async function getById(id) {
    return await getProcess(id);
}

async function create(params) {
    // validate
    if (await db.Process.findOne({ where: { name: params.name } })) {
        throw 'Process "' + params.name + '" is already registered';
    }
    await process.save();

}

async function update(id, params) {
    const process = await getProcess(id);

    // validate
    const nameChanged = params.name && process.name !== params.name;
    if (nameChanged && await db.Process.findOne({ where: { name: params.name } })) {
        throw 'Process "' + params.name + '" is already registered';
    }

    // copy params to process and save
    Object.assign(process, params);
    await process.save();
}

async function _delete(id) {
    const process = await getProcess(id);
    await process.destroy();
}

// helper functions

async function getProcess(id) {
    const process = await db.Process.findByPk(id);
    if (!process) throw 'Process not found';
    return process;
}
