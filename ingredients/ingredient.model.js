const { DataTypes } = require('sequelize');
const Joi = require("joi");

module.exports = ingredient;

function ingredient(sequelize) {

    const attributes = {
        id: {type: DataTypes.INTEGER, allowNull: false, primaryKey: true, autoIncrement: true},
        name: { type: DataTypes.STRING, allowNull: false },
        description: { type: DataTypes.STRING, allowNull: false },
    };

    return sequelize.define('Ingredient', attributes);
}
