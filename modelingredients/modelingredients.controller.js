const express = require('express');
const router = express.Router();
const Joi = require('joi');

const validateRequest = require('_middleware/validate-request');
const modelIngredientService = require('./modelingredient.service');
const verifyToken = require('../users/user.service');
// routes

router.get('/', getIngredientByModel);
router.get('/:id', getById);
router.delete('/:id', _delete);

module.exports = router;

// route functions

function getAll(req, res, next) {
    modelIngredientService.getAll()
        .then(modelIngredient => res.json(modelIngredient))
        .catch(next);
}

function getById(req, res, next) {
    modelIngredientService.getById(req.params.id)
        .then(modelIngredient => res.json(modelIngredient))
        .catch(next);
}

function create(req, res, next) {
    modelIngredientService.create(req.body)
        .then(() => res.json({ message: 'modelIngredient created' }))
        .catch(next);
}

function update(req, res, next) {
    modelIngredientService.update(req.params.id, req.body)
        .then(() => res.json({ message: 'modelIngredient updated' }))
        .catch(next);
}

function _delete(req, res, next) {
    modelIngredientService.delete(req.params.id)
        .then(() => res.json({ message: 'modelIngredient deleted' }))
        .catch(next);
}

function getIngredientByModel(req,res,next){
    modelIngredientService.getIngredientByModel()
        .then(modelIngredient => res.json(modelIngredient))
        .catch(next);
}
