const bcrypt = require('bcryptjs');

const db = require('_helpers/db');

module.exports = {
    getAll,
    getById,
    create,
    update,
    getIngredientByModel,
    delete: _delete
};

async function getAll() {
    return await db.ModelIngredient.findAll();
}

async function getById(id) {
    return await getModelIngredient(id);
}

async function create(params) {
    // validate
    if (await db.ModelIngredient.findOne({ where: { name: params.name } })) {
        throw 'ModelIngredient "' + params.name + '" is already registered';
    }
    const modelIngredient = new db.ModelIngredient(params);

    // save modelIngredient
    await modelIngredient.save();
}

async function update(id, params) {
    const modelIngredient = await getModelIngredient(id);

    // validate
    const nameChanged = params.name && modelIngredient.name !== params.name;
    if (nameChanged && await db.ModelIngredient.findOne({ where: { name: params.name } })) {
        throw 'ModelIngredient "' + params.name + '" is already registered';
    }

    // copy params to modelIngredient and save
    Object.assign(modelIngredient, params);
    await modelIngredient.save();
}

async function _delete(id) {
    const modelIngredient = await getModelIngredient(id);
    await modelIngredient.destroy();
}

// helper functions

async function getModelIngredient(id) {
    const modelIngredient = await db.ModelIngredient.findByPk(id);
    if (!modelIngredient) throw 'ModelIngredient not found';
    return modelIngredient;
}
async function getIngredientByModel() {
    const models = await db.Model.findAll({include: db.Ingredient});
    return models;
}
