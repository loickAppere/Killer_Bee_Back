const express = require('express');
const router = express.Router();
const Joi = require('joi');

const validateRequest = require('_middleware/validate-request');
const modelService = require('./model.service');
const {DataTypes} = require("sequelize");
const verifyToken = require('../users/user.service');

// routes

router.get('/', verifyToken.verifyToken, getAll);
router.get('/:id', verifyToken.verifyToken, getById);
router.post('/', createSchema, create);
router.put('/:id', updateSchema, update);
router.delete('/:id', _delete);

module.exports = router;

// route functions

function getAll(req, res, next) {
    modelService.getAll()
        .then(models => res.json(models))
        .catch(next);
}

function getById(req, res, next) {
    modelService.getById(req.params.id)
        .then(model => res.json(model))
        .catch(next);
}

function create(req, res, next) {
    modelService.create(req.body)
        .then(() => res.json({ message: 'model created' }))
        .catch(next);
}

function update(req, res, next) {
    modelService.update(req.params.id, req.body)
        .then(() => res.json({ message: 'model updated' }))
        .catch(next);
}

function _delete(req, res, next) {
    modelService.delete(req.params.id)
        .then(() => res.json({ message: 'model deleted' }))
        .catch(next);
}

// schema functions

function createSchema(req, res, next) {
    const schema = Joi.object({
        name: Joi.string().required(),
        description: Joi.string().allow(''),
        upet: Joi.number().positive().precision(2).required(),
        range: Joi.string().required(),
        IngredientId: Joi.array().items(Joi.number()).allow(null),
    });
    validateRequest(req, next, schema);
}

function updateSchema(req, res, next) {
    const schema = Joi.object({
        name: Joi.string().empty(''),
        description: Joi.string().empty(''),
        upet: Joi.number().empty(''),
        range: Joi.string().empty(''),
        IngredientId: Joi.array().items(Joi.number()).empty('')

    });
    validateRequest(req, next, schema);
}
