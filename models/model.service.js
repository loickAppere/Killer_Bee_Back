const bcrypt = require('bcryptjs');

const db = require('_helpers/db');
const modelIngredientService = require('../modelingredients/modelingredient.service');

module.exports = {
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

async function getAll() {
    return await db.Model.findAll();
}

async function getById(id) {
    return await getModel(id);
}

async function create(params) {
    if (await db.Model.findOne({ where: { name: params.name } })) {
        throw 'Model "' + params.name + '" is already registered';
    }
    let paramsIngredient = { ...params.IngredientId }
    delete params.IngredientId;

    const model = new db.Model(params);
    await model.save();
    for(let ing in paramsIngredient){
        let modelIngredient = new db.ModelIngredient({IngredientId: ing,ModelId: model.id});
        await modelIngredient.save();
    }
}

async function update(id, params) {
    let paramsIngredient = [ ...params.IngredientId ]
    delete params.IngredientId;
    const model = await getModel(id);

    // validate
    const nameChanged = params.name && model.name !== params.name;
    if (nameChanged && await db.Model.findOne({ where: { name: params.name } })) {
        throw 'Model "' + params.name + '" is already registered';
    }

    // copy params to model and save
    Object.assign(model, params);
    await model.save();
    // modify ingredients join to model
    let allModelIngredient = await db.ModelIngredient.findAll({where: { ModelId: model.id} });
    let allModelIngredientId=[];
    let allIngredientId=[]
    for(let i=0; i<allModelIngredient.length;i++){
        allModelIngredientId.push(allModelIngredient[i].modelingredientId);
        if(!allIngredientId.includes(allModelIngredient[i].IngredientId))
            allIngredientId.push(allModelIngredient[i].IngredientId);
        if(!paramsIngredient.includes(allModelIngredient[i].IngredientId)){

            await modelIngredientService.delete(allModelIngredient[i].modelingredientId);
       }
    }
    for(let ing in paramsIngredient){
        if(!allIngredientId.includes(parseInt(ing))){
            let modelIngredient = await new db.ModelIngredient({IngredientId: ing, ModelId: model.id});
            await modelIngredient.save();
        }
    }
}

async function _delete(id) {
    const model = await getModel(id);
    await model.destroy();
}

// helper functions

async function getModel(id) {
    const model = await db.Model.findByPk(id);
    if (!model) throw 'Model not found';
    return model;
}
