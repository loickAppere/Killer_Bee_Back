const { DataTypes } = require('sequelize');
const Joi = require("joi");
const {number} = require("joi");

module.exports = model;

function model(sequelize) {

    const attributes = {
        id: {type: DataTypes.INTEGER, allowNull: false, primaryKey: true, autoIncrement: true},
        name: { type: DataTypes.STRING, allowNull: false },
        description: { type: DataTypes.STRING, allowNull: true },
        upet: { type: DataTypes.FLOAT, allowNull: false },
        range: { type: DataTypes.STRING, allowNull: false },
        IngredientId: { type: DataTypes.INTEGER, allowNull: true}
    };

    return sequelize.define('Model', attributes);
}
