const tedious = require('tedious');
const { Sequelize, DataTypes} = require('sequelize');
const { dbName, dbConfig } = require('config.json');
const express = require("express");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

module.exports = db = {};

initialize();



async function initialize() {
    const dialect = 'mssql';
    const host = dbConfig.server;
    const { userName, password } = dbConfig.authentication.options;

    // create db if it doesn't already exist
    await ensureDbExists(dbName);

    // connect to db
    const sequelize = new Sequelize(dbName, userName, password, { host, dialect });

    const ModelIngredient = sequelize.define('modelingredient',{
        modelingredientId: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        IngredientId: {
            type: DataTypes.INTEGER
        },
        ModelId: {
            type: DataTypes.INTEGER
        }
    })

    // init models and add them to the exported db object
    db.User = require('../users/user.model')(sequelize);
    db.Model = require('../models/model.model')(sequelize);
    db.Ingredient = require('../ingredients/ingredient.model')(sequelize);
    db.ModelIngredient = ModelIngredient;
    db.Process = require('../processes/process.model')(sequelize)

    // associations (FK key)
    db.Ingredient.belongsToMany(db.Model, {through: ModelIngredient});
    db.Model.belongsToMany(db.Ingredient, {through: ModelIngredient});
    db.Process.hasOne(db.Model, );
    db.Model.belongsTo(db.Process);


    // sync all models with database
    await sequelize.sync({ alter: true });
}

async function ensureDbExists(dbName) {
    return new Promise((resolve, reject) => {
        const connection = new tedious.Connection(dbConfig);
        connection.connect((err) => {
            if (err) {
                console.error(err);
                reject(`Connection Failed: ${err.message}`);
            }

            const createDbQuery = `IF NOT EXISTS(SELECT * FROM sys.databases WHERE name = '${dbName}') CREATE DATABASE [${dbName}];`;
            const request = new tedious.Request(createDbQuery, (err) => {
                if (err) {
                    console.error(err);
                    reject(`Create DB Query Failed: ${err.message}`);
                }

                // query executed successfully
                resolve();
            });

            connection.execSql(request);
        });
    });
}
