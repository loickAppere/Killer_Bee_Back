const express = require('express');
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const userService = require("../users/user.service");
const router = express.Router();
const dotenv = require("dotenv")

const verifyToken = require("../_middleware/VerifyToken");
const refreshToken = require("../_middleware/RefreshToken");


router.post('/authenticate', authenticate);
router.get('/token', refreshTokens);

async function refreshTokens(){
    await refreshToken.refreshToken();
}
async function authenticate(req, res, next) {
    try {
        const user = await db.User.findAll({
            where:{
                email: req.body.email
            }
        });
        req.body.password= userService.hashPassword(req.body.password);
        //if(!req.body.password.equals(user[0].password)) return res.status(400).json({msg: "Wrong Password"});

        const userId = user[0].id;
        const firstName = user[0].firstName;
        const email = user[0].email;
        const lastName = user[0].lastName;

        const accessToken = jwt.sign({userId, firstName, email, lastName}, process.env.ACCESS_TOKEN_SECRET,{
            expiresIn: '20s'
        });
        const refreshToken = jwt.sign({userId, firstName, email, lastName}, process.env.REFRESH_TOKEN_SECRET,{
            expiresIn: '1d'
        });
        await db.User.update({refresh_token: refreshToken},{
            where:{
                id: userId
            }
        });
        res.cookie('refreshToken', refreshToken,{
            httpOnly: true,
            maxAge: 24 * 60 * 60 * 1000
        });
        res.json({ accessToken });
    } catch (error) {
        res.status(404).json({ msg:error.body });
    }
}

module.exports = router;
