﻿require('rootpath')();
const express = require('express');
const app = express();
const dotenv = require("dotenv");
const cookieParser = require("cookie-parser");
dotenv.config();
const cors = require('cors');

const errorHandler = require('_middleware/error-handler');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors(
    {
        origin: '*'
    }
));
app.use(cookieParser());

// api routes
app.use('/users', require('./users/users.controller'));
app.use('/models', require('./models/models.controller'));
app.use('/ingredients', require('./ingredients/ingredients.controller'));
app.use('/modelIngredient', require('./modelingredients/modelingredients.controller'));
app.use('/processes', require('./processes/processes.controller'));
app.use('/', require('./login/login.controller'))


// global error handler
app.use(errorHandler);

// start server
const port = process.env.NODE_ENV === 'production' ? (process.env.PORT || 80) : 4000;
app.listen(port, () => console.log('Server listening on port ' + port));
